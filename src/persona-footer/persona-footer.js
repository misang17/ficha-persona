import { LitElement, html, css } from 'lit-element';

class PersonaFooter extends LitElement {

    static get styles() {
		return css`
		h5{ display: flex;
        justify-content: center;
        border: 1px solid black;
        background-color: #28a745;
        margin-bottom: 0;
    }	
        `;
	}

    render() {
        return html`
            <h5>@Persona App 2021</h5>
        `;
    }
}

customElements.define('persona-footer', PersonaFooter)