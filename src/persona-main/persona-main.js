import { LitElement, html, css } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js'; 
class PersonaMain extends LitElement {

	static get styles() {
		return css`
		persona-ficha-listado{
			margin-top: 10px;
		}
			`;
	}

	static get properties() {
		return {
			people: { type: Array },
			showPersonForm: {type: Boolean}
		};
	}

	constructor() {
		super();

		this.people = [
			{
				name: "Ellen Ripley",
				yearsInCompany: 10,
				profile: "Lorem ipsum dolor sit amet.",
				photo: {
					"src": "./img/contact.png",
					"alt": "Ellen Ripley"
				},
				id: 1
			}, {
				name: "Bruce Banner",
				yearsInCompany: 2,
				profile: "Lorem ipsum.",
				photo: {
					"src": "./img/contact.png",
					"alt": "Bruce Banner"
				},
				id: 2
			}, {
				name: "Éowyn",
				yearsInCompany: 5,
				profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
				photo: {
					"src": "./img/contact.png",
					"alt": "Éowyn"
				},
				id: 3
			}, {
				name: "Turanga Leela",
				yearsInCompany: 9,
				profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod.",
				photo: {
					"src": "./img/contact.png",
					"alt": "Turanga Leela"
				},
				id: 4
			}, {
				name: "Tyrion Lannister",
				yearsInCompany: 1,
				profile: "Lorem ipsum.",
				photo: {
					"src": "./img/contact.png",
					"alt": "Tyrion Lannister"
				},
				id: 5
			}	
		];
		this.showPersonForm = false;
	}

	updated(changedProperties) { 
		if (changedProperties.has("showPersonForm")) {
			if (this.showPersonForm === true) {
				this.showPersonFormData();
			} else {
				this.showPersonList();
			}
		}
	}

	render() {
		return html`
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
			<h2>Personas</h2>
			<div class="row" id="peopleList">		
				<div class="row row-cols-1 row-cols-sm-4">
					${this.people.map(
						person =>
						html`<persona-ficha-listado 
							name="${person.name}" 
							yearsInCompany="${person.yearsInCompany}" 
							profile="${person.profile}" 
							.photo="${person.photo}"
							id="${person.id}"
							@delete-person="${this.deletePerson}">
						</persona-ficha-listado>`
		)}
			</div>
		</div>
		<div class="row">
		<persona-form id="personForm" class="d-none border rounded border-primary"
			@persona-form-close="${this.personFormClose}"
			@persona-form-store="${this.personFormStore}" >
		</persona-form>
		</div>					
		`;
	}

	deletePerson(e) {
		this.people = this.people.filter(
			person => person.id != e.detail.id
		);
	}

	showPersonList() {
		this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
		this.shadowRoot.getElementById("personForm").classList.add("d-none");	
		}

	showPersonFormData() {
		this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
		this.shadowRoot.getElementById("peopleList").classList.add("d-none");	
	}

	personFormClose() {
		this.showPersonForm = false;
	}

	personFormStore(e) {
		e.detail.person.id = this.getId();
		this.people.push(e.detail.person);
		this.showPersonForm = false;
	}

	getId(){
        let mayor = 0;
        this.people.forEach(person => {
            if (person.id > mayor) {
				mayor = person.id;
			}
        })
        return parseInt(mayor) + 1;
    }
}
customElements.define('persona-main', PersonaMain)
