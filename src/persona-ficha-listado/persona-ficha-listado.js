import { LitElement, html, css } from 'lit-element';
class PersonaFichaListado extends LitElement {

	static get styles() {
		return css`
			.card-text{
			font-size:19px;
			width:200px;
			height:100px;
			overflow:auto;
			}
			.card-title{
				border-bottom: 1px solid green;
			}
			.card-footer{
				display: flex;
				flex-direction: column;
				align-items: center;
			}
			`;
	}

	static get properties() {
		return {
			name: { type: String },
			yearsInCompany: { type: Number },
			profile: { type: String },
			photo: { type: Object },
			id: { type: Number }
		};
	}

	constructor() {
		super();
	}


	render() {
		return html`
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">	
			<div class="card h-100 m-2">
				<img src="${this.photo.src}" alt="${this.photo.alt}" height="200" width="50" class="card-img-top">
				<div class="card-body">
					<h5 class="card-title">${this.name}</h5>
					<p class="card-text">${this.profile}</p>
					<ul class="list-group list-group-flush">
						<li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
					</ul>
				</div>
			<div class="card-footer">
				<button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>X</strong></button>
				<p>Eliminar</p>
			</div>				
		</div>
	`;
	}

	deletePerson(e) {
		this.dispatchEvent(
			new CustomEvent("delete-person", {
				detail: {
					id: this.id
				}
			}
			)
		);
	}
}
customElements.define('persona-ficha-listado', PersonaFichaListado)