import { LitElement, html, css } from 'lit-element';

class PersonaHeader extends LitElement {


	static get styles() {
		return css`
		h1{ display: flex;
        justify-content: center;
        border: 1px solid black;
        background-color: #28a745;
        margin-top: 0;
    }	
        `;
	}

    render() {
        return html`
            <h1>Directorio Personas</h1>
        `;
    }
}

customElements.define('persona-header', PersonaHeader)