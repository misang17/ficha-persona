import { LitElement, html, css } from 'lit-element' 
import '../persona-header/persona-header'; 
import '../persona-main/persona-main';
import '../persona-footer/persona-footer';
import '../persona-sidebar/persona-sidebar'; 
class PersonaApp extends LitElement {
	static get styles() {
		return css`
		.body-main{
			overflow: hidden;
			margin-bottom: 5%;
		}
    }	
        `;
	}

	render() {
		return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
			<persona-header></persona-header>
			<div class="row body-main">
				<persona-sidebar class="col-2" @new-person="${this.newPerson}"></persona-sidebar>
				<persona-main class="col-10"></persona-main>
			</div>			
			<persona-footer></persona-footer>
		`;
	}
    newPerson(e) {
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

	} 
customElements.define('persona-app', PersonaApp);