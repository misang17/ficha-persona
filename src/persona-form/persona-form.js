import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {
    static get properties() {
        return {
            person: { type: Object }
        };
    }

    constructor() {
        super();
        this.person = {};
    }

    render() {
        return html`	
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">		
			<div>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input type="text" @input="${this.updateName}" id="personFormName" class="form-control" placeholder="Nombre Completo"/>
                    <div>
                    <div class="form-group">
                      <label>Perfil</label>
                       <textarea @input="${this.updateProfile}" class="form-control" placeholder="Perfil" rows="5"></textarea>
                    <div>
                    <div class="form-group">
                    <label>Años en la empresa</label>
                    <input type="number" @input="${this.updateYearsInCompany}" class="form-control" placeholder="Años en la empresa"/>
                    <div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
			</div>
		`;
    }

    updateName(e) {
        this.person.name = e.target.value;
    }

    updateProfile(e) {
        this.person.profile = e.target.value;
    }
    updateYearsInCompany(e) {
        this.person.yearsInCompany = e.target.value;
    }


    goBack(e) {
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("persona-form-close", {}));
    }

    storePerson(e) {
        let validation = true;
        let inputForm = this.shadowRoot.querySelectorAll('.form-control');
        inputForm.forEach(inp => {
            if(inp.value === "" || inp.value === undefined){
                validation = false;
                this.formBorderColor(inp, false);
            }else{
                this.formBorderColor(inp, true);
            }
        })
        if(validation){        
        e.preventDefault();
        
        this.person.photo = {
            "src": "./img/contact.png",
            "alt": "Persona"
        };
            
        this.dispatchEvent(new CustomEvent("persona-form-store",{
            detail: {
                person:  {
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    }
                }
            })
        );

       
        inputForm.forEach(inp => {
            inp.value = "";
        })
    }     
    }

    formBorderColor(inp, validate){
        if(validate){
            inp.style.border = "1px solid #ced4da";
        }else{
            inp.style.border = "3px solid red";
        }
    }

}
customElements.define('persona-form', PersonaForm)